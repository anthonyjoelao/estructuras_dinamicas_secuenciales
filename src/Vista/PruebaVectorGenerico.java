/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;
import util.ufps.colecciones_seed.VectorGenerico;

/**
 *
 * @author madarme
 */
public class PruebaVectorGenerico {
    
    public static void main(String[] args) {
        try{
            
            VectorGenerico<Integer> v3=new VectorGenerico(3);
            v3.add(3); //posición=0
            v3.add(-4);//posición=1
            v3.add(1);//posición=2
            //v3.add(6);//posición=NO error :(--> sólo tiene 3 en capacidad
            //v3.sort();
            VectorGenerico<Integer> v4=new VectorGenerico(3);
            v4.add(13); //posición=0
            v4.add(-4);//posición=1
           // v4.add(1);//posición=2
            
            if (v3.equals(v4))
                System.out.println(":) Son iguales vectores de Integer");
            else
                System.out.println(":( NO Son iguales vectores de Integer");
            
            
            System.out.println("Mi vector de enteros tiene :"+v3.length());
            System.out.println("Mi vector de enteros :"+v3.toString());
            System.out.println("El elemento 2 después de ordenado es :"+v3.get(2));
            
            VectorGenerico<Punto> v2=new VectorGenerico(15);
            v2.add(new Punto(3,4));
            v2.add(new Punto(4,5));
            //v2.set(12, new Punto(-1,-1));
            //System.out.println("Mi vector de puntos :"+v2.toString());
            
            
            
        }catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
    }
}
