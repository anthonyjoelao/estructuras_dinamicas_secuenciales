/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class ListaS<T> {
    
    //Inicialización OPCIONAL, SE REALIZA POR CONCEPTO
    private Nodo<T> cabeza=null;
    private int tamanio=0;

    public ListaS() {
    }

    public int getTamanio() {
        return tamanio;
    }

    /**
     * Método para insertar un elemento nuevo en el inicio de la  lista simple 
     * @param datoNuevo un objeto para la listaS
     */
    public void insertarInicio(T datoNuevo)
    {
        /**
         * Estrategia computacional(Algoritmo):
            1. Crear nodoNuevo=new (dato,cabeza);
            2. aumentar tamanio
            3. Cambiar cabeza a nuevoNodo
         */
            
        Nodo<T> nodoNuevo=new Nodo(datoNuevo, this.cabeza);
        this.cabeza=nodoNuevo;
        this.tamanio++;
        
    }
    
    
    public void insertarFin(T datoNuevo)
    {
        if(this.esVacia())
            this.insertarInicio(datoNuevo);
        else
        {
            Nodo<T> nodoNuevo=new Nodo(datoNuevo, null);
            
            try {
                Nodo<T> nodoUltimo=getPos(this.tamanio-1);
                nodoUltimo.setSig(nodoNuevo);
                this.tamanio++;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
    
    
    public T get(int i)
    {
    
        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
    
    
    public void set(int i, T datoNuevo)
    {
    
        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            
        }
    }
    
    /**
     * Método elimina el nodo que se encuentra en una posición dada
     * @param i posición del nodo en la lista simple
     * @return el objeto almacenado en el nodo con posición i
     */
    public T eliminar(int i)
    {
     if(this.esVacia())
         return null;
     Nodo<T> nodoBorrar=null;
     if(i==0)
     {
         nodoBorrar=this.cabeza;
         this.cabeza=this.cabeza.getSig();
     }
     else
     {
         try {
             Nodo<T> nodoAnt=this.getPos(i-1);
             nodoBorrar=nodoAnt.getSig();
             nodoAnt.setSig(nodoBorrar.getSig());
             
         } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
         }
     }
     nodoBorrar.setSig(null);
     this.tamanio--;
     return nodoBorrar.getInfo();
    }
    
    
    private Nodo<T> getPos(int i) throws Exception
    {
    if(i<0 || i >=this.tamanio)
            throw new Exception("Índice fuera de rango");
    
    Nodo<T> nodoPos=this.cabeza;
   
    while(i-->0)
    {
        nodoPos=nodoPos.getSig();
        
    }
    return nodoPos;
    }
    
    @Override
    public String toString() {
        String msg="";
        
        for(Nodo<T> nodoActual=this.cabeza;nodoActual!=null;nodoActual=nodoActual.getSig())
        {
            msg+=nodoActual.getInfo().toString()+"->";
        }
        return "Cabeza->"+msg+"null";
        
    }
    
    public boolean esVacia()
    {
      return this.cabeza==null;
    }
    
    /**
     * Elimina de la lista todo dato que esté repetido (NO SE DEJA NINGUNO)
     * NO SE PUEDE LLAMAR N VECES AL MÉTODO REPETIR
     * NO SE PUEDE USAR EL MÉTODO GET O SET
     * NO SE PUEDE USAR EL MÉTODO GETPOS
     * 
     * Ejemplo: L=<2,3,4,2,1,9,2,1,3>
     *  L.eliminarRepetido()--> L=<4,9>
     */
    public void eliminarRepetido()
    {
        // :)
    }       
    
    
    
    
    
    
}
